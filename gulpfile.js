var browserSync = require('browser-sync').create();
var gulp = require('gulp');
var rimraf = require('rimraf');
var run = require('run-sequence');

var dirs = {
    build: './build/',
    source: './src/'
};

var tasks = {
    clean: {
        dir: dirs.build
    },
    html: {
        files: [dirs.source + 'index.html'],
        dest: dirs.build
    },
    css: {
        files: [dirs.source + 'css/**/*.css'],
        dest: dirs.build + 'css'
    },
    js: {
        files: [dirs.source + 'js/**/*.js'],
        dest: dirs.build + 'js'
    }
};

gulp.task('clean', function (cb) {
    return rimraf(tasks.clean.dir, cb);
});

gulp.task('html', function () {
    return gulp.src(tasks.html.files).pipe(gulp.dest(tasks.html.dest));
});

gulp.task('css', function () {
    return gulp.src(tasks.css.files).pipe(gulp.dest(tasks.css.dest));
});

gulp.task('js', function () {
    return gulp.src(tasks.js.files).pipe(gulp.dest(tasks.js.dest));
});

gulp.task('serve', function () {
    browserSync.init({
        server: {
            baseDir: dirs.build
        }
    });

    gulp.watch(tasks.html.files, ['html', browserSync.reload]);
    gulp.watch(tasks.css.files, ['css', browserSync.reload]);
    gulp.watch(tasks.js.files, ['js', browserSync.reload]);
});

gulp.task('build', function (cb) {
    run('clean', 'html', 'css', 'js', cb);
});

gulp.task('default', ['build'], function (cb) {
    run('serve', cb);
});
