(function () {
    'use strict';

    NES.M6502 = function (memory) {
        this.a = 0;
        this.x = 0;
        this.y = 0;
        this.pc = 0;
        this.sp = 0xfd;
        this.status = 0x24;
        this.cycles = 0;
        this.extraCycles = 0;
        this.memory = memory;
        this.address = 0;
        this.data = null;

        this.pc = this.memory.readVector(VECTOR.RESET);
    }

    NES.M6502.prototype.fetch = function () {
        var address = this.pc;

        this.pc = (this.pc + 1) & 0xffff;

        return this.memory.read(address);
    }

    NES.M6502.prototype.handleNMI = function () {
        this.pushToStack((this.pc >> 8) & 0xff);
        this.pushToStack(this.pc & 0xff);
        this.pushToStack(this.status | 0x20 & 0xff);
        this.setFlag(FLAG.I);
        this.pc = this.memory.readVector(VECTOR.NMI);
    }

    NES.M6502.prototype.getNextOpcode = function () {
        return this.fetch();
    }

    NES.M6502.prototype.executeOpcode = function (opcode) {
        var fn;

        this.data = OPCODE_DATA[opcode];
        fn = 'op_' + this.data.name;

        if (this[fn]) {
            this.extraCycles = 0;
            this.solveAddress();
            this[fn]();
            this.cycles = this.data.cycles + this.extraCycles;
        } else {
            throw new Error(
                'invalid opcode <' + ((this.pc - 1) & 0xffff).toHexadecimal() + ':' + opcode.toHexadecimal() + '>'
            );
        }

        return this.cycles;
    }

    NES.M6502.prototype.solveAddress = function (opcode) {
        var mode = this.data.mode;
        var h;
        var l;
        var offset;

        switch (mode) {
            case 'Absolute': {
                l = this.fetch();
                h = this.fetch();
                this.address = Helper.makeWord(l, h);
            }; break;

            case 'Absolute,X': {
                l = this.fetch();
                h = this.fetch();
                this.address = (Helper.makeWord(l, h) + this.x) & 0xffff;
            }; break;

            case 'Absolute,X*': {
                l = this.fetch();
                h = this.fetch();
                this.address = (Helper.makeWord(l, h) + this.x) & 0xffff;

                if (l + this.x > 0xff) {
                    this.extraCycles += 1;
                }
            }; break;

            case 'Absolute,Y': {
                l = this.fetch();
                h = this.fetch();
                this.address = (Helper.makeWord(l, h) + this.y) & 0xffff;
            }; break;

            case 'Absolute,Y*': {
                l = this.fetch();
                h = this.fetch();
                this.address = (Helper.makeWord(l, h) + this.y) & 0xffff;

                if (l + this.y > 0xff) {
                    this.extraCycles += 1;
                }
            }; break;

            case 'Accumulator': {}; break;

            case 'Immediate': {
                this.address = this.pc;
                this.pc = (this.pc + 1) & 0xffff;
            }; break;

            case 'Implied': {}; break;

            case 'Indirect': {
                l = this.fetch();
                h = this.fetch();
                offset = Helper.makeWord(l, h);
                this.address = this.memory.read(offset);
                offset = Helper.makeWord((l + 1) & 0xff, h);
                this.address = ((this.memory.read(offset) << 8) | this.address) & 0xffff;
            }; break;

            case 'Relative': {
                offset = this.fetch() & 0xff;
                this.extraCycles += 1;

                if (Helper.testBit(offset, 7)) {
                    offset -= 0x100;
                }

                this.address = (this.pc + offset) & 0xffff;

                if ((this.address & 0xff00) !== (this.pc & 0xff00)) {
                    this.extraCycles += 1;
                }
            }; break;

            case 'Zero Page': {
                this.address = this.fetch();
            }; break;

            case 'Zero Page,X': {
                this.address = (this.fetch() + this.x) & 0xff;
            }; break;

            case 'Zero Page,Y': {
                this.address = (this.fetch() + this.y) & 0xff;
            }; break;

            case '(Indirect,X)': {
                offset = (this.fetch() + this.x) & 0xff;
                l = this.memory.read(offset);
                h = this.memory.read((offset + 1) & 0xff);
                this.address = Helper.makeWord(l, h);
            }; break;

            case '(Indirect),Y': {
                offset = this.fetch();
                l = this.memory.read(offset);
                h = this.memory.read((offset + 1) & 0xff);
                this.address = (Helper.makeWord(l, h) + this.y) & 0xffff;
            }; break;

            case '(Indirect),Y*': {
                offset = this.fetch();
                l = this.memory.read(offset);
                h = this.memory.read((offset + 1) & 0xff);
                this.address = (Helper.makeWord(l, h) + this.y) & 0xffff;

                if (l + this.y > 0xff) {
                    this.extraCycles += 1;
                }
            }; break;

            default: {
                throw new Error('invalid addressing mode <' + mode + '>');
            };
        }
    }

    NES.M6502.prototype.pushToStack = function (value) {
        this.memory.write((this.sp + 0x100) & 0xfff, value);
        this.sp = (this.sp - 1) & 0xff;
    }

    NES.M6502.prototype.popFromStak = function () {
        this.sp = (this.sp + 1) & 0xff;

        return this.memory.read((this.sp + 0x100) & 0xfff);
    }

    NES.M6502.prototype.clearFlag = function (flag) {
        this.status = this.status & (~flag) & 0xff;
    }

    NES.M6502.prototype.setFlag = function (flag) {
        this.status = this.status | flag & 0xff;
    }

    NES.M6502.prototype.testFlag = function (flag) {
        return ((this.status & flag) === flag);
    }

    NES.M6502.prototype.updateZN = function (value) {
        this.clearFlag(FLAG.Z | FLAG.N);
        this.setFlag(ZN[value]);
    }

    NES.M6502.prototype.op_ADC = function () {
        var c1;
        var c2;
        var value = this.memory.read(this.address);
        var carry = (this.testFlag(FLAG.C)) ? 1 : 0;
        var result = (this.a + value + carry) & 0xffff;

        this.clearFlag(FLAG.C | FLAG.V);

        if (result > 0xff) {
            this.setFlag(FLAG.C);
        }

        result &= 0xff;
        c1 = Helper.testBit(this.a ^ value, 7);
        c2 = Helper.testBit(this.a ^ result, 7);

        if (!c1 && c2) {
            this.setFlag(FLAG.V);
        }

        this.a = result;
        this.updateZN(this.a);
    }

    NES.M6502.prototype.op_AND = function () {
        this.a = this.a & this.memory.read(this.address) & 0xff;
        this.updateZN(this.a);
    }

    NES.M6502.prototype.op_ASL = function () {
        var isAccumulatorMode = (this.data.mode === 'Accumulator');
        var value = (isAccumulatorMode) ? this.a : this.memory.read(this.address);

        this.clearFlag(FLAG.C);

        if (Helper.testBit(value, 7)) {
            this.setFlag(FLAG.C);
        }

        value = (value << 1) & 0xfe;
        this.updateZN(value);

        if (isAccumulatorMode) {
            this.a = value;
        } else {
            this.memory.write(this.address, value);
        }
    }

    NES.M6502.prototype.op_BCC = function () {
        if (!this.testFlag(FLAG.C)) {
            this.pc = this.address;
        } else {
            this.extraCycles = 0;
        }
    }

    NES.M6502.prototype.op_BCS = function () {
        if (this.testFlag(FLAG.C)) {
            this.pc = this.address;
        } else {
            this.extraCycles = 0;
        }
    }

    NES.M6502.prototype.op_BEQ = function () {
        if (this.testFlag(FLAG.Z)) {
            this.pc = this.address;
        } else {
            this.extraCycles = 0;
        }
    }

    NES.M6502.prototype.op_BIT = function () {
        var value = this.memory.read(this.address);

        this.updateZN(value & this.a);
        this.clearFlag(FLAG.N | FLAG.V);

        if (Helper.testBit(value, 7)) {
            this.setFlag(FLAG.N);
        }

        if (Helper.testBit(value, 6)) {
            this.setFlag(FLAG.V);
        }
    }

    NES.M6502.prototype.op_BMI = function () {
        if (this.testFlag(FLAG.N)) {
            this.pc = this.address;
        } else {
            this.extraCycles = 0;
        }
    }

    NES.M6502.prototype.op_BNE = function () {
        if (!this.testFlag(FLAG.Z)) {
            this.pc = this.address;
        } else {
            this.extraCycles = 0;
        }
    }

    NES.M6502.prototype.op_BPL = function () {
        if (!this.testFlag(FLAG.N)) {
            this.pc = this.address;
        } else {
            this.extraCycles = 0;
        }
    }

    NES.M6502.prototype.op_BVC = function () {
        if (!this.testFlag(FLAG.V)) {
            this.pc = this.address;
        } else {
            this.extraCycles = 0;
        }
    }

    NES.M6502.prototype.op_BVS = function () {
        if (this.testFlag(FLAG.V)) {
            this.pc = this.address;
        } else {
            this.extraCycles = 0;
        }
    }

    NES.M6502.prototype.op_CLC = function () {
        this.clearFlag(FLAG.C);
    }

    NES.M6502.prototype.op_CLD = function () {
        this.clearFlag(FLAG.D);
    }

    NES.M6502.prototype.op_CLV = function () {
        this.clearFlag(FLAG.V);
    }

    NES.M6502.prototype.op_CMP = function () {
        var src = this.memory.read(this.address);

        this.updateZN((this.a - src) & 0xff);
        this.clearFlag(FLAG.C);

        if (this.a >= src) {
            this.setFlag(FLAG.C);
        }
    }

    NES.M6502.prototype.op_CPX = function () {
        var src = this.memory.read(this.address);

        this.updateZN((this.x - src) & 0xff);
        this.clearFlag(FLAG.C);

        if (this.x >= src) {
            this.setFlag(FLAG.C);
        }
    }

    NES.M6502.prototype.op_CPY = function () {
        var src = this.memory.read(this.address);

        this.updateZN((this.y - src) & 0xff);
        this.clearFlag(FLAG.C);

        if (this.y >= src) {
            this.setFlag(FLAG.C);
        }
    }

    NES.M6502.prototype.op_DEC = function () {
        var value = this.memory.read(this.address);

        value = (value - 1) & 0xff;
        this.memory.write(this.address, value);
        this.updateZN(value);
    }

    NES.M6502.prototype.op_DEX = function () {
        this.x = (this.x - 1) & 0xff;
        this.updateZN(this.x);
    }

    NES.M6502.prototype.op_DEY = function () {
        this.y = (this.y - 1) & 0xff;
        this.updateZN(this.y);
    }

    NES.M6502.prototype.op_EOR = function () {
        this.a = this.a ^ this.memory.read(this.address) & 0xff;
        this.updateZN(this.a);
    }

    NES.M6502.prototype.op_INC = function () {
        var value = this.memory.read(this.address);

        value = (value + 1) & 0xff
        this.memory.write(this.address, value);
        this.updateZN(value);
    }

    NES.M6502.prototype.op_INX = function () {
        this.x = (this.x + 1) & 0xff;
        this.updateZN(this.x);
    }

    NES.M6502.prototype.op_INY = function () {
        this.y = (this.y + 1) & 0xff;
        this.updateZN(this.y);
    }

    NES.M6502.prototype.op_JMP = function () {
        this.pc = this.address;
    }

    NES.M6502.prototype.op_JSR = function () {
        var address = (this.pc - 1) & 0xffff;

        this.pushToStack((address >> 8) & 0xff);
        this.pushToStack(address & 0xff);
        this.pc = this.address;
    }

    NES.M6502.prototype.op_LDA = function () {
        this.a = this.memory.read(this.address);
        this.updateZN(this.a);
    }

    NES.M6502.prototype.op_LDX = function () {
        this.x = this.memory.read(this.address);
        this.updateZN(this.x);
    }

    NES.M6502.prototype.op_LDY = function () {
        this.y = this.memory.read(this.address);
        this.updateZN(this.y);
    }

    NES.M6502.prototype.op_LSR = function () {
        var isAccumulatorMode = (this.data.mode === 'Accumulator');
        var value = (isAccumulatorMode) ? this.a : this.memory.read(this.address);

        this.clearFlag(FLAG.C);

        if (Helper.testBit(value, 0)) {
            this.setFlag(FLAG.C);
        }

        value = (value >> 1) & 0x7f;
        this.updateZN(value);

        if (isAccumulatorMode) {
            this.a = value;
        } else {
            this.memory.write(this.address, value);
        }
    }

    NES.M6502.prototype.op_NOP = function () {}

    NES.M6502.prototype.op_ORA = function () {
        this.a = this.a | this.memory.read(this.address) & 0xff;
        this.updateZN(this.a);
    }

    NES.M6502.prototype.op_PHA = function () {
        this.pushToStack(this.a);
    }

    NES.M6502.prototype.op_PHP = function () {
        this.pushToStack(this.status | 0x30 & 0xff);
    }

    NES.M6502.prototype.op_PLA = function () {
        this.a = this.popFromStak();
        this.updateZN(this.a);
    }

    NES.M6502.prototype.op_PLP = function () {
        this.status = this.popFromStak();
        this.clearFlag(FLAG.B);
        this.setFlag(FLAG.P);
    }

    NES.M6502.prototype.op_ROL = function () {
        var isAccumulatorMode = (this.data.mode === 'Accumulator');
        var value = (isAccumulatorMode) ? this.a : this.memory.read(this.address);
        var isOldCarrySet = this.testFlag(FLAG.C);

        this.clearFlag(FLAG.C);

        if (Helper.testBit(value, 7)) {
            this.setFlag(FLAG.C);
        }

        value = (value << 1) & 0xfe;

        if (isOldCarrySet) {
            value = value | 0x1 & 0xff;
        }

        this.updateZN(value);

        if (isAccumulatorMode) {
            this.a = value;
        } else {
            this.memory.write(this.address, value);
        }
    }

    NES.M6502.prototype.op_ROR = function () {
        var isAccumulatorMode = (this.data.mode === 'Accumulator');
        var value = (isAccumulatorMode) ? this.a : this.memory.read(this.address);
        var isOldCarrySet = this.testFlag(FLAG.C);

        this.clearFlag(FLAG.C);

        if (Helper.testBit(value, 0)) {
            this.setFlag(FLAG.C);
        }

        value = (value >> 1) & 0x7f;

        if (isOldCarrySet) {
            value = value | 0x80 & 0xff;
        }

        this.updateZN(value);

        if (isAccumulatorMode) {
            this.a = value;
        } else {
            this.memory.write(this.address, value);
        }
    }

    NES.M6502.prototype.op_RTI = function () {
        var l;
        var h;

        this.status = this.popFromStak();
        this.clearFlag(FLAG.B);
        this.setFlag(FLAG.P);

        l = this.popFromStak();
        h = this.popFromStak();
        this.pc = Helper.makeWord(l, h);
    }

    NES.M6502.prototype.op_RTS = function () {
        var l = this.popFromStak();
        var h = this.popFromStak();

        this.pc = (Helper.makeWord(l, h) + 1) & 0xffff;
    }

    NES.M6502.prototype.op_SBC = function () {
        var c1;
        var c2;
        var value = this.memory.read(this.address);
        var carry = (this.testFlag(FLAG.C)) ? 0 : 1;
        var result = (this.a - value - carry) & 0xffff;

        this.clearFlag(FLAG.C | FLAG.V);

        if (result < 0x100) {
            this.setFlag(FLAG.C);
        }

        result &= 0xff;
        c1 = Helper.testBit(this.a ^ value, 7);
        c2 = Helper.testBit(this.a ^ result, 7);

        if (c1 && c2) {
            this.setFlag(FLAG.V);
        }

        this.a = result;
        this.updateZN(this.a);
    }

    NES.M6502.prototype.op_SEC = function () {
        this.setFlag(FLAG.C);
    }

    NES.M6502.prototype.op_SED = function () {
        this.setFlag(FLAG.D);
    }

    NES.M6502.prototype.op_SEI = function () {
        this.setFlag(FLAG.I);
    }

    NES.M6502.prototype.op_STA = function () {
        this.memory.write(this.address, this.a);
    }

    NES.M6502.prototype.op_STX = function () {
        this.memory.write(this.address, this.x);
    }

    NES.M6502.prototype.op_STY = function () {
        this.memory.write(this.address, this.y);
    }

    NES.M6502.prototype.op_TSX = function () {
        this.x = this.sp;
        this.updateZN(this.x);
    }

    NES.M6502.prototype.op_TXA = function () {
        this.a = this.x;
        this.updateZN(this.a);
    }

    NES.M6502.prototype.op_TXS = function () {
        this.sp = this.x;
    }

    NES.M6502.prototype.op_TYA = function () {
        this.a = this.y;
        this.updateZN(this.a);
    }

    NES.M6502.prototype.op_TAX = function () {
        this.x = this.a;
        this.updateZN(this.x);
    }

    NES.M6502.prototype.op_TAY = function () {
        this.y = this.a;
        this.updateZN(this.y);
    }

} ());
