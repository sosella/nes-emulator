var OPCODE_DATA = {
    0x4C: {
        name: 'JMP',
        mode: 'Absolute',
        size: 3,
        cycles: 3
    },
    0x6C: {
        name: 'JMP',
        mode: 'Indirect',
        size: 3,
        cycles: 5
    },
    0xA2: {
        name: 'LDX',
        mode: 'Immediate',
        size: 2,
        cycles: 2
    },
    0xA6: {
        name: 'LDX',
        mode: 'Zero Page',
        size: 2,
        cycles: 3
    },
    0xB6: {
        name: 'LDX',
        mode: 'Zero Page,Y',
        size: 2,
        cycles: 4
    },
    0xAE: {
        name: 'LDX',
        mode: 'Absolute',
        size: 3,
        cycles: 4
    },
    0xBE: {
        name: 'LDX',
        mode: 'Absolute,Y*',
        size: 3,
        cycles: 4
    },
    0x86: {
        name: 'STX',
        mode: 'Zero Page',
        size: 2,
        cycles: 3
    },
    0x96: {
        name: 'STX',
        mode: 'Zero Page,Y',
        size: 2,
        cycles: 4
    },
    0x8E: {
        name: 'STX',
        mode: 'Absolute',
        size: 3,
        cycles: 4
    },
    0x20: {
        name: 'JSR',
        mode: 'Absolute',
        size: 3,
        cycles: 6
    },
    0xEA: {
        name: 'NOP',
        mode: 'Implied',
        size: 1,
        cycles: 2
    },
    0x38: {
        name: 'SEC',
        mode: 'Implied',
        size: 1,
        cycles: 2,
    },
    0xB0: {
        name: 'BCS',
        mode: 'Relative',
        size: 2,
        cycles: 2
    },
    0x18: {
        name: 'CLC',
        mode: 'Implied',
        size: 1,
        cycles: 2
    },
    0x90: {
        name: 'BCC',
        mode: 'Relative',
        size: 2,
        cycles: 2
    },
    0xA9: {
        name: 'LDA',
        mode: 'Immediate',
        size: 2,
        cycles: 2
    },
    0xA5: {
        name: 'LDA',
        mode: 'Zero Page',
        size: 2,
        cycles: 3
    },
    0xB5: {
        name: 'LDA',
        mode: 'Zero Page,X',
        size: 2,
        cycles: 4
    },
    0xAD: {
        name: 'LDA',
        mode: 'Absolute',
        size: 3,
        cycles: 4
    },
    0xBD: {
        name: 'LDA',
        mode: 'Absolute,X*',
        size: 3,
        cycles: 4
    },
    0xB9: {
        name: 'LDA',
        mode: 'Absolute,Y*',
        size: 3,
        cycles: 4
    },
    0xA1: {
        name: 'LDA',
        mode: '(Indirect,X)',
        size: 2,
        cycles: 6
    },
    0xB1: {
        name: 'LDA',
        mode: '(Indirect),Y*',
        size: 2,
        cycles: 5
    },
    0xF0: {
        name: 'BEQ',
        mode: 'Relative',
        size: 2,
        cycles: 2
    },
    0xD0: {
        name: 'BNE',
        mode: 'Relative',
        size: 2,
        cycles: 2
    },
    0x85: {
        name: 'STA',
        mode: 'Zero Page',
        size: 2,
        cycles: 3
    },
    0x95: {
        name: 'STA',
        mode: 'Zero Page,X',
        size: 2,
        cycles: 4
    },
    0x8D: {
        name: 'STA',
        mode: 'Absolute',
        size: 3,
        cycles: 4
    },
    0x9D: {
        name: 'STA',
        mode: 'Absolute,X',
        size: 3,
        cycles: 5
    },
    0x99: {
        name: 'STA',
        mode: 'Absolute,Y',
        size: 3,
        cycles: 5
    },
    0x81: {
        name: 'STA',
        mode: '(Indirect,X)',
        size: 2,
        cycles: 6
    },
    0x91: {
        name: 'STA',
        mode: '(Indirect),Y',
        size: 2,
        cycles: 6
    },
    0x24: {
        name: 'BIT',
        mode: 'Zero Page',
        size: 2,
        cycles: 3
    },
    0x2C: {
        name: 'BIT',
        mode: 'Absolute',
        size: 3,
        cycles: 4
    },
    0x70: {
        name: 'BVS',
        mode: 'Relative',
        size: 2,
        cycles: 2
    },
    0x50: {
        name: 'BVC',
        mode: 'Relative',
        size: 2,
        cycles: 2
    },
    0x10: {
        name: 'BPL',
        mode: 'Relative',
        size: 2,
        cycles: 2
    },
    0x60: {
        name: 'RTS',
        mode: 'Implied',
        size: 1,
        cycles: 6
    },
    0x78: {
        name: 'SEI',
        mode: 'Implied',
        size: 1,
        cycles: 2
    },
    0xF8: {
        name: 'SED',
        mode: 'Implied',
        size: 1,
        cycles: 2
    },
    0x08: {
        name: 'PHP',
        mode: 'Implied',
        size: 1,
        cycles: 3
    },
    0x68: {
        name: 'PLA',
        mode: 'Implied',
        size: 1,
        cycles: 4
    },
    0x29: {
        name: 'AND',
        mode: 'Immediate',
        size: 2,
        cycles: 2
    },
    0x25: {
        name: 'AND',
        mode: 'Zero Page',
        size: 2,
        cycles: 3
    },
    0x35: {
        name: 'AND',
        mode: 'Zero Page,X',
        size: 2,
        cycles: 4
    },
    0x2D: {
        name: 'AND',
        mode: 'Absolute',
        size: 3,
        cycles: 4
    },
    0x3D: {
        name: 'AND',
        mode: 'Absolute,X*',
        size: 3,
        cycles: 4
    },
    0x39: {
        name: 'AND',
        mode: 'Absolute,Y*',
        size: 3,
        cycles: 4
    },
    0x21: {
        name: 'AND',
        mode: '(Indirect,X)',
        size: 2,
        cycles: 6
    },
    0x31: {
        name: 'AND',
        mode: '(Indirect),Y*',
        size: 2,
        cycles: 5
    },
    0xC9: {
        name: 'CMP',
        mode: 'Immediate',
        size: 2,
        cycles: 2
    },
    0xC5: {
        name: 'CMP',
        mode: 'Zero Page',
        size: 2,
        cycles: 3
    },
    0xD5: {
        name: 'CMP',
        mode: 'Zero Page,X',
        size: 2,
        cycles: 4
    },
    0xCD: {
        name: 'CMP',
        mode: 'Absolute',
        size: 3,
        cycles: 4
    },
    0xDD: {
        name: 'CMP',
        mode: 'Absolute,X*',
        size: 3,
        cycles: 4
    },
    0xD9: {
        name: 'CMP',
        mode: 'Absolute,Y*',
        size: 3,
        cycles: 4
    },
    0xC1: {
        name: 'CMP',
        mode: '(Indirect,X)',
        size: 2,
        cycles: 6
    },
    0xD1: {
        name: 'CMP',
        mode: '(Indirect),Y*',
        size: 2,
        cycles: 5
    },
    0xD8: {
        name: 'CLD',
        mode: 'Implied',
        size: 1,
        cycles: 2
    },
    0x48: {
        name: 'PHA',
        mode: 'Implied',
        size: 1,
        cycles: 3
    },
    0x28: {
        name: 'PLP',
        mode: 'Implied',
        size: 1,
        cycles: 4
    },
    0x30: {
        name: 'BMI',
        mode: 'Relative',
        size: 2,
        cycles: 2
    },
    0x09: {
        name: 'ORA',
        mode: 'Immediate',
        size: 2,
        cycles: 2
    },
    0x05: {
        name: 'ORA',
        mode: 'Zero Page',
        size: 2,
        cycles: 3
    },
    0x15: {
        name: 'ORA',
        mode: 'Zero Page,X',
        size: 2,
        cycles: 4
    },
    0x0D: {
        name: 'ORA',
        mode: 'Absolute',
        size: 3,
        cycles: 4
    },
    0x1D: {
        name: 'ORA',
        mode: 'Absolute,X*',
        size: 3,
        cycles: 4
    },
    0x19: {
        name: 'ORA',
        mode: 'Absolute,Y*',
        size: 3,
        cycles: 4
    },
    0x01: {
        name: 'ORA',
        mode: '(Indirect,X)',
        size: 2,
        cycles: 6
    },
    0x11: {
        name: 'ORA',
        mode: '(Indirect),Y*',
        size: 2,
        cycles: 5
    },
    0xB8: {
        name: 'CLV',
        mode: 'Implied',
        size: 1,
        cycles: 2
    },
    0x49: {
        name: 'EOR',
        mode: 'Immediate',
        size: 2,
        cycles: 2
    },
    0x45: {
        name: 'EOR',
        mode: 'Zero Page',
        size: 2,
        cycles: 3
    },
    0x55: {
        name: 'EOR',
        mode: 'Zero Page,X',
        size: 2,
        cycles: 4
    },
    0x4D: {
        name: 'EOR',
        mode: 'Absolute',
        size: 3,
        cycles: 4
    },
    0x5D: {
        name: 'EOR',
        mode: 'Absolute,X*',
        size: 3,
        cycles: 4
    },
    0x59: {
        name: 'EOR',
        mode: 'Absolute,Y*',
        size: 3,
        cycles: 4
    },
    0x41: {
        name: 'EOR',
        mode: '(Indirect,X)',
        size: 2,
        cycles: 6
    },
    0x51: {
        name: 'EOR',
        mode: '(Indirect),Y*',
        size: 2,
        cycles: 5
    },
    0x69: {
        name: 'ADC',
        mode: 'Immediate',
        size: 2,
        cycles: 2
    },
    0x65: {
        name: 'ADC',
        mode: 'Zero Page',
        size: 2,
        cycles: 3
    },
    0x75: {
        name: 'ADC',
        mode: 'Zero Page,X',
        size: 2,
        cycles: 4
    },
    0x6D: {
        name: 'ADC',
        mode: 'Absolute',
        size: 3,
        cycles: 4
    },
    0x7D: {
        name: 'ADC',
        mode: 'Absolute,X*',
        size: 3,
        cycles: 4
    },
    0x79: {
        name: 'ADC',
        mode: 'Absolute,Y*',
        size: 3,
        cycles: 4
    },
    0x61: {
        name: 'ADC',
        mode: '(Indirect,X)',
        size: 2,
        cycles: 6
    },
    0x71: {
        name: 'ADC',
        mode: '(Indirect),Y*',
        size: 2,
        cycles: 5
    },
    0xA0: {
        name: 'LDY',
        mode: 'Immediate',
        size: 2,
        cycles: 2
    },
    0xA4: {
        name: 'LDY',
        mode: 'Zero Page',
        size: 2,
        cycles: 3
    },
    0xB4: {
        name: 'LDY',
        mode: 'Zero Page,X',
        size: 2,
        cycles: 4
    },
    0xAC: {
        name: 'LDY',
        mode: 'Absolute',
        size: 3,
        cycles: 4
    },
    0xBC: {
        name: 'LDY',
        mode: 'Absolute,X*',
        size: 3,
        cycles: 4
    },
    0xC0: {
        name: 'CPY',
        mode: 'Immediate',
        size: 2,
        cycles: 2
    },
    0xC4: {
        name: 'CPY',
        mode: 'Zero Page',
        size: 2,
        cycles: 3
    },
    0xCC: {
        name: 'CPY',
        mode: 'Absolute',
        size: 3,
        cycles: 4
    },
    0xE0: {
        name: 'CPX',
        mode: 'Immediate',
        size: 2,
        cycles: 2
    },
    0xE4: {
        name: 'CPX',
        mode: 'Zero Page',
        size: 2,
        cycles: 3
    },
    0xEC: {
        name: 'CPX',
        mode: 'Absolute',
        size: 3,
        cycles: 4
    },
    0xE9: {
        name: 'SBC',
        mode: 'Immediate',
        size: 2,
        cycles: 2
    },
    0xE5: {
        name: 'SBC',
        mode: 'Zero Page',
        size: 2,
        cycles: 3
    },
    0xF5: {
        name: 'SBC',
        mode: 'Zero Page,X',
        size: 2,
        cycles: 4
    },
    0xED: {
        name: 'SBC',
        mode: 'Absolute',
        size: 3,
        cycles: 4
    },
    0xFD: {
        name: 'SBC',
        mode: 'Absolute,X*',
        size: 3,
        cycles: 4
    },
    0xF9: {
        name: 'SBC',
        mode: 'Absolute,Y*',
        size: 3,
        cycles: 4
    },
    0xE1: {
        name: 'SBC',
        mode: '(Indirect,X)',
        size: 2,
        cycles: 6
    },
    0xF1: {
        name: 'SBC',
        mode: '(Indirect),Y*',
        size: 2,
        cycles: 5
    },
    0xC8: {
        name: 'INY',
        mode: 'Implied',
        size: 1,
        cycles: 2
    },
    0xE8: {
        name: 'INX',
        mode: 'Implied',
        size: 1,
        cycles: 2
    },
    0x88: {
        name: 'DEY',
        mode: 'Implied',
        size: 1,
        cycles: 2
    },
    0xCA: {
        name: 'DEX',
        mode: 'Implied',
        size: 1,
        cycles: 2
    },
    0xA8: {
        name: 'TAY',
        mode: 'Implied',
        size: 1,
        cycles: 2
    },
    0xAA: {
        name: 'TAX',
        mode: 'Implied',
        size: 1,
        cycles: 2
    },
    0x98: {
        name: 'TYA',
        mode: 'Implied',
        size: 1,
        cycles: 2
    },
    0x8A: {
        name: 'TXA',
        mode: 'Implied',
        size: 1,
        cycles: 2
    },
    0xBA: {
        name: 'TSX',
        mode: 'Implied',
        size: 1,
        cycles: 2
    },
    0x9A: {
        name: 'TXS',
        mode: 'Implied',
        size: 1,
        cycles: 2
    },
    0x40: {
        name: 'RTI',
        mode: 'Implied',
        size: 1,
        cycles: 6
    },
    0x4A: {
        name: 'LSR',
        mode: 'Accumulator',
        size: 1,
        cycles: 2
    },
    0x46: {
        name: 'LSR',
        mode: 'Zero Page',
        size: 2,
        cycles: 5
    },
    0x56: {
        name: 'LSR',
        mode: 'Zero Page,X',
        size: 2,
        cycles: 6
    },
    0x4E: {
        name: 'LSR',
        mode: 'Absolute',
        size: 3,
        cycles: 6
    },
    0x5E: {
        name: 'LSR',
        mode: 'Absolute,X',
        size: 3,
        cycles: 7
    },
    0x0A: {
        name: 'ASL',
        mode: 'Accumulator',
        size: 1,
        cycles: 2
    },
    0x06: {
        name: 'ASL',
        mode: 'Zero Page',
        size: 2,
        cycles: 5
    },
    0x16: {
        name: 'ASL',
        mode: 'Zero Page,X',
        size: 2,
        cycles: 6,
    },
    0x0E: {
        name: 'ASL',
        mode: 'Absolute',
        size: 3,
        cycles: 6
    },
    0x1E: {
        name: 'ASL',
        mode: 'Absolute,X',
        size: 3,
        cycles: 7
    },
    0x6A: {
        name: 'ROR',
        mode: 'Accumulator',
        size: 1,
        cycles: 2
    },
    0x66: {
        name: 'ROR',
        mode: 'Zero Page',
        size: 2,
        cycles: 5
    },
    0x76: {
        name: 'ROR',
        mode: 'Zero Page,X',
        size: 2,
        cycles: 6
    },
    0x6E: {
        name: 'ROR',
        mode: 'Absolute',
        size: 3,
        cycles: 6
    },
    0x7E: {
        name: 'ROR',
        mode: 'Absolute,X',
        size: 3,
        cycles: 7
    },
    0x2A: {
        name: 'ROL',
        mode: 'Accumulator',
        size: 1,
        cycles: 2
    },
    0x26: {
        name: 'ROL',
        mode: 'Zero Page',
        size: 2,
        cycles: 5
    },
    0x36: {
        name: 'ROL',
        mode: 'Zero Page,X',
        size: 2,
        cycles: 6
    },
    0x2E: {
        name: 'ROL',
        mode: 'Absolute',
        size: 3,
        cycles: 6
    },
    0x3E: {
        name: 'ROL',
        mode: 'Absolute,X',
        size: 3,
        cycles: 7
    },
    0x84: {
        name: 'STY',
        mode: 'Zero Page',
        size: 2,
        cycles: 3
    },
    0x94: {
        name: 'STY',
        mode: 'Zero Page,X',
        size: 2,
        cycles: 4
    },
    0x8C: {
        name: 'STY',
        mode: 'Absolute',
        size: 3,
        cycles: 4
    },
    0xE6: {
        name: 'INC',
        mode: 'Zero Page',
        size: 2,
        cycles: 5
    },
    0xF6: {
        name: 'INC',
        mode: 'Zero Page,X',
        size: 2,
        cycles: 6
    },
    0xEE: {
        name: 'INC',
        mode: 'Absolute',
        size: 3,
        cycles: 6
    },
    0xFE: {
        name: 'INC',
        mode: 'Absolute,X',
        size: 3,
        cycles: 7
    },
    0xC6: {
        name: 'DEC',
        mode: 'Zero Page',
        size: 2,
        cycles: 5
    },
    0xD6: {
        name: 'DEC',
        mode: 'Zero Page,X',
        size: 2,
        cycles: 6
    },
    0xCE: {
        name: 'DEC',
        mode: 'Absolute',
        size: 3,
        cycles: 6
    },
    0xDE: {
        name: 'DEC',
        mode: 'Absolute,X',
        size: 3,
        cycles: 7
    }
};
