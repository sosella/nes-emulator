(function () {
    'use strict';

    NES.PPU = function (memory) {
        this.memory = memory;
        this.bus = 0;
        this.addressLatch = false;
        this.scanline = -1;
        this.nmiPending = false;
        this.vramAddressIncrement = 0;
        this.buffer = 0;
        this.screen = new Array(SCREEN.width * SCREEN.height);
        this.backgroundEnabled = false;
        this.spritesEnabled = false;
        this.backgroundPatternTableAddress = 0;
        this.nameTableAddress = 0;
        this.registers = {
            PPUCTRL: 0,
            PPUMASK: 0,
            PPUSTATUS: 0,
            OAMADDR: 0,
            OAMDATA: 0,
            PPUSCROLL: 0,
            PPUDATA: 0,
            OAMDMA: 0
        };
        this.loopy = {v: null, t: null};
    }

    NES.PPU.prototype.read = function (register) {
        var value;

        switch (register) {
            case 2: {
                value = this.readPPUSTATUS();
                this.bus = value;
            }; break;

            case 4: {
                // TODO
                console.log('unimplemented PPU read $' + register);
                this.bus = value;
            }; break;

            case 7: {
                // TODO
                value = this.readPPUDATA()
                this.bus = value;
            }; break;

            default: {
                console.log('unimplemented PPU read $' + register);
                value = this.bus;
            }; break;
        }

        return value;
    }

    NES.PPU.prototype.write = function (register, value) {
        this.bus = value;

        switch (register) {
            case 0: {
                this.writePPUCTRL(value);
            }; break;

            case 1: {
                this.writePPUMASK(value);
            }; break;

            case 3: {
                this.writeOAMADDR(value);
            }; break;

            case 5: {
                this.writePPUSCROLL(value);
            }; break;

            case 6: {
                this.writePPUADDR(value);
            }; break;

            case 7: {
                this.writePPUDATA(value);
            }; break;

            default: {
                throw new Error('invalid PPU write $' + register + ':' + value.toHexadecimal(2));
            }; break;
        }
    }

    // $2002
    NES.PPU.prototype.readPPUSTATUS = function () {
        // http://wiki.nesdev.com/w/index.php/PPU_registers
        // 7  bit  0
        // ---- ----
        // VSO. ....
        // |||| ||||
        // |||+-++++- Least significant bits previously written into a PPU register
        // |||        (due to register not being updated for this address)
        // ||+------- Sprite overflow. The intent was for this flag to be set
        // ||         whenever more than eight sprites appear on a scanline, but a
        // ||         hardware bug causes the actual behavior to be more complicated
        // ||         and generate false positives as well as false negatives; see
        // ||         PPU sprite evaluation. This flag is set during sprite
        // ||         evaluation and cleared at dot 1 (the second dot) of the
        // ||         pre-render line.
        // |+-------- Sprite 0 Hit.  Set when a nonzero pixel of sprite 0 overlaps
        // |          a nonzero background pixel; cleared at dot 1 of the pre-render
        // |          line.  Used for raster timing.
        // +--------- Vertical blank has started (0: not in vblank; 1: in vblank).
        //            Set at dot 1 of line 241 (the line *after* the post-render
        //            line); cleared after reading $2002 and at dot 1 of the
        //            pre-render line.

        var value = this.registers.PPUSTATUS & 0xe0;

        value |= this.bus & 0x1f;
        this.registers.PPUSTATUS &= 0x7f;
        this.addressLatch = false;

        return value;
    }

    // $2007
    NES.PPU.prototype.readPPUDATA = function () {
        var value;
        var address = this.loopy.v & 0x3fff;

        if (address <= 0x3eff) {
            value = this.buffer;
            this.buffer = this.memory.readVRAM(address);
        } else {
            value = this.memory.readVRAM(address);
            this.buffer = this.memory.readVRAM((address - 0x1000) & 0x3fff);
        }

        this.loopy.v = (this.loopy.v + this.vramAddressIncrement) & 0x7fff;

        return value;
    }

    // $2000
    NES.PPU.prototype.writePPUCTRL = function (value) {
        // http://wiki.nesdev.com/w/index.php/PPU_registers
        // 7  bit  0
        // ---- ----
        // VPHB SINN
        // |||| ||||
        // |||| ||++- Base nametable address
        // |||| ||    (0 = $2000; 1 = $2400; 2 = $2800; 3 = $2C00)
        // |||| |+--- VRAM address increment per CPU read/write of PPUDATA
        // |||| |     (0: add 1, going across; 1: add 32, going down)
        // |||| +---- Sprite pattern table address for 8x8 sprites
        // ||||       (0: $0000; 1: $1000; ignored in 8x16 mode)
        // |||+------ Background pattern table address (0: $0000; 1: $1000)
        // ||+------- Sprite size (0: 8x8; 1: 8x16)
        // |+-------- PPU master/slave select
        // |          (0: read backdrop from EXT pins; 1: output color on EXT pins)
        // +--------- Generate an NMI at the start of the
        //            vertical blanking interval (0: off; 1: on)

        this.registers.PPUCTRL = value;
        this.vramAddressIncrement = Helper.testBit(value, 2) ? 32 : 1;
        this.nameTableAddress = ((value & 0x3) * 0x400 + 0x2000) & 0x3fff;
        this.backgroundPatternTableAddress = Helper.testBit(value, 4) ? 0x1000 : 0;

        this.loopy.t &= 0x73ff;
        this.loopy.t = this.loopy.t | ((value & 0x3) << 10) & 0x7fff;
    }

    // $2001
    NES.PPU.prototype.writePPUMASK = function (value) {
        // http://wiki.nesdev.com/w/index.php/PPU_registers
        // 7  bit  0
        // ---- ----
        // BGRs bMmG
        // |||| ||||
        // |||| |||+- Greyscale (0: normal color, 1: produce a greyscale display)
        // |||| ||+-- 1: Show background in leftmost 8 pixels of screen, 0: Hide
        // |||| |+--- 1: Show sprites in leftmost 8 pixels of screen, 0: Hide
        // |||| +---- 1: Show background
        // |||+------ 1: Show sprites
        // ||+------- Emphasize red*
        // |+-------- Emphasize green*
        // +--------- Emphasize blue*

        this.registers.PPUMASK = value;
        this.backgroundEnabled = Helper.testBit(value, 3);
        this.spritesEnabled = Helper.testBit(value, 4);
    }

    // $2003
    NES.PPU.prototype.writeOAMADDR = function (value) {
        this.registers.OAMADDR = value;
    }

    // $2005
    NES.PPU.prototype.writePPUSCROLL = function (value) {
        if (!this.addressLatch) {
            this.loopy.t &= 0x7fe0;
            this.loopy.t = this.loopy.t | ((value & 0xf8) >> 3) & 0x7fff;
        } else {
            this.loopy.t &= 0x0c1f;
            this.loopy.t = this.loopy.t | ((value & 0x7) << 12) | ((value & 0xf8) << 2) & 0x7fff;
        }

        this.addressLatch = (!this.addressLatch);
    }

    // $2006
    NES.PPU.prototype.writePPUADDR = function (value) {
        if (!this.addressLatch) {
            this.loopy.t &= 0xff;
            this.loopy.t = this.loopy.t | ((value & 0x3f) << 8) & 0x7fff;
        } else {
            this.loopy.t &= 0x7f00;
            this.loopy.t = this.loopy.t | value & 0x7fff;
            this.loopy.v = this.loopy.t;
        }

        this.addressLatch = (!this.addressLatch);
    }

    // $2007
    NES.PPU.prototype.writePPUDATA = function (value) {
        var address = this.loopy.v & 0x3fff;

        this.memory.writeVRAM(address, value);
        this.loopy.v = (this.loopy.v + this.vramAddressIncrement) & 0x7fff;
    }

    NES.PPU.prototype.updateVBlankFlag = function (value) {
        this.registers.PPUSTATUS &= 0x7f;

        if (value) {
            this.registers.PPUSTATUS |= 0x80;
        }
    }

    NES.PPU.prototype.updateNMIPending = function () {
        this.nmiPending = false;

        if (this.generateNMI()) {
            this.nmiPending = true;
        }
    }

    NES.PPU.prototype.wantNMI = function () {
        var result = false;

        if (this.nmiPending) {
            this.nmiPending = false;
            result = true;
        }

        return result;
    }

    NES.PPU.prototype.generateNMI = function () {
        return Helper.testBit(this.registers.PPUCTRL, 7);
    }

    NES.PPU.prototype.getScreenBuffer = function () {
        return this.screen.slice(0);
    }

    NES.PPU.prototype.isRenderingEnabled = function () {
        return (this.backgroundEnabled || this.spritesEnabled);
    }

    NES.PPU.prototype.renderScanline = function () {
        // pre-render scanline
        if (this.scanline === -1) {
            if (this.isRenderingEnabled()) {
                this.loopy.v = this.loopy.t;
            }
        }

        // visible scanlines
        else if (this.scanline >= 0 && this.scanline <= 239) {
            this.scrollAndRender();
        }

        // post-render scanline
        else if (this.scanline === 240) {}

        // start of vertical blanking
        else if (this.scanline === 241) {
            this.updateNMIPending();
            this.updateVBlankFlag(true);
        }

        // end of vertical blanking
         else if (this.scanline === 260) {
            this.scanline = -2;
            this.updateVBlankFlag(false);
        }

        this.scanline++;
    }

    NES.PPU.prototype.scrollAndRender = function () {
        var coarseY;

        if (this.isRenderingEnabled()) {
            this.loopy.v &= 0x7be0;
            this.loopy.v = this.loopy.v | (this.loopy.t & 0x41f) & 0x7fff;

            this.renderBackground();

            if ((this.loopy.v & 0x7000) !== 0x7000) {
                this.loopy.v = (this.loopy.v + 0x1000) & 0x7fff;
            } else {
                this.loopy.v &= 0xfff;
                coarseY = (this.loopy.v >> 5) & 0x1f;

                if (coarseY === 0x1d) {
                    coarseY = 0;
                    this.loopy.v = (this.loopy.v ^ 0x800) & 0x7fff;
                } else if (coarseY === 0x1f) {
                    coarseY = 0;
                } else {
                    coarseY = (coarseY + 1) & 0xff;
                }

                this.loopy.v &= 0x7c1f;
                this.loopy.v = (this.loopy.v | (coarseY << 5)) & 0x7fff;
            }
        }
    }

    NES.PPU.prototype.renderBackground = function () {
        var pattern;
        var attribute;
        var colour;

        for (var tile = 0; tile < 32; tile++) {
            pattern = this.fetchPattern();
            attribute = this.fetchAttribute();

            for (var bit = 7; bit >= 0; bit--) {
                colour = this.getPaletteEntry(pattern, attribute, bit);
                this.screen[this.scanline * SCREEN.width + tile * 8 + 7 - bit] = PALETTE[colour];
            }

            if ((this.loopy.v & 0x1f) === 0x1f) {
                this.loopy.v &= 0x7fe0;
                this.loopy.v = (this.loopy.v ^ 0x400) & 0x7fff;
            } else {
                this.loopy.v = (this.loopy.v + 1) & 0x7fff;
            }
        }
    }

    NES.PPU.prototype.fetchPattern = function () {
        var number = this.memory.readVRAM(this.nameTableAddress + (this.loopy.v & 0xfff) & 0x3fff);
        var fineY = (this.loopy.v >> 12) & 0x7;
        var address = (this.backgroundPatternTableAddress + number * 16 + fineY) & 0x3fff;

        return {
            l: this.memory.readVRAM(address),
            h: this.memory.readVRAM((address + 8) & 0x3fff)
        };
    }

    NES.PPU.prototype.fetchAttribute = function () {
        var baseAddress = 0x23c0 + (this.loopy.v & 0xc00);
        var address = baseAddress + ((this.loopy.v >> 4) & 0x38) + ((this.loopy.v >> 2) & 0x7);
        var attribute = this.memory.readVRAM(address & 0x3fff);

        attribute &= ATTRIBUTE_MASK[this.loopy.v & 0x3ff];
        attribute >>= ATTRIBUTE_SHIFT[this.loopy.v & 0x3ff];

        return attribute;
    }

    NES.PPU.prototype.getPaletteEntry = function (pattern, attribute, bit) {
        var l = (pattern.l >> bit) & 0x1;
        var h = (pattern.h >> bit) & 0x1;
        var address = ((attribute << 2) | (h << 1) | l & 0x3) + 0x3f00;

        return this.memory.readVRAM(address & 0x3fff);
    }

} ());
