(function () {
    'use strictt';

    NES.Console = function (args) {
        this.running = false;
        this.rom = new Uint8Array(0x10000);
        this.vram = new Uint8Array(0x4000);
        this.sram = new Uint8Array(0x100);
        this.mirroring = 0;

        this.allocate(args.rom);

        this.memory = {
            peekROM: this.peekROM.bind(this),
            peekVRAM: this.peekVRAM.bind(this),
            read: this.read.bind(this),
            readVector: this.readVector.bind(this),
            readVRAM: this.readVRAM.bind(this),
            write: this.write.bind(this),
            writeVRAM: this.writeVRAM.bind(this)
        }
        this.m6502 = new NES.M6502(this.memory);
        this.ppu = new NES.PPU(this.memory);
        this.debugger = new NES.Debugger({
            m6502: this.m6502,
            names: args.names,
            patterns: args.patterns
        });
        this.screen = {
            canvas: document.getElementById(args.screen),
            height: SCREEN.height,
            width: SCREEN.width
        };

        this.screen.ctx = this.screen.canvas.getContext('2d');
        this.screen.imageData = this.screen.ctx.createImageData(this.screen.width, this.screen.height);
    }

    NES.Console.prototype.start = function () {
        var opcode;
        var scanlinesTimming = [114, 114, 113];
        var cycles = 0;
        var scanlinesPerFrame = 262;
        var mainLoop;

        this.running = true;

        mainLoop = setInterval(function () {
            if (!this.running) {
                clearInterval(mainLoop);
            }

            for (var scanline = 0; scanline < scanlinesPerFrame; scanline++) {
                while (cycles < scanlinesTimming[scanline % scanlinesTimming.length]) {
                    opcode = this.m6502.getNextOpcode();
                    cycles += this.m6502.executeOpcode(opcode);
                }

                cycles -= scanlinesTimming[scanline % scanlinesTimming.length];

                this.ppu.renderScanline();

                if (this.ppu.wantNMI()) {
                    this.m6502.handleNMI();
                }
            }

            this.draw();
            this.debugger.showPatterns();
            this.debugger.showNames();
        }.bind(this), 1000 / 60);
    }

    NES.Console.prototype.stop = function () {
        this.running = false;
    }

    NES.Console.prototype.allocate = function (data) {
        var romBanks = data[4];
        var offset = 16;

        this.mirroring = Helper.testBit(data[6], 0) ? MIRRORING.VERTICAL : MIRRORING.HORIZONTAL;

        if (romBanks === 1) {
            for (var i = 0; i < 0x4000; i++) {
                this.rom[0x8000 + i] = this.rom[0xc000 + i] = data[offset++];
            }
        } else if (romBanks === 2) {
            for (var i = 0; i < 0x8000; i++) {
                this.rom[0x8000 + i] = data[offset++];
            }
        }

        for (var i = 0; i < 0x2000; i++) {
            this.vram[i] = data[offset++];
        }
    }

    NES.Console.prototype.peekROM = function (address) {
        return this.rom[address];
    }

    NES.Console.prototype.peekVRAM = function (address) {
        return this.vram[address];
    }

    NES.Console.prototype.readVector = function (vector) {
        var l = this.read(vector.l);
        var h = this.read(vector.h);

        return Helper.makeWord(l, h);
    }

    NES.Console.prototype.read = function (address) {
        var value;

        // internal RAM
        if (address <= 0x1fff) {
            value = this.rom[address & 0x7ff];
        }

        // PPU
        else if (address >= 0x2000 && address <= 0x3fff) {
            value = this.ppu.read(address & 0x7);
        }

        // fallback
        else {
            value = this.rom[address];
        }

        return value;
    }

    NES.Console.prototype.write = function (address, value) {
        // internal RAM
        if (address <= 0x1fff) {
            this.rom[address & 0x7ff] = value;
        }

        // PPU
        else if (address >= 0x2000 && address <= 0x3fff) {
            this.ppu.write(address & 0x7, value);
        }

        // fallback
        else {
            this.rom[address] = value;
        }
    }

    NES.Console.prototype.readVRAM = function (address) {
        var value;

        address &= 0x3fff;

        // name tables
        if (address >= 0x2000 && address <= 0x3eff) {
            value = this.vram[address & 0x2fff];
        }

        // palette
        else if (address >= 0x3f00 && address <= 0x3fff) {
            value = this.vram[address];
        }

        // fallback
        else {
            value = this.vram[address];
        }

        return value;
    }

    NES.Console.prototype.writeVRAM = function (address, value) {
        var baseAddress;

        address &= 0x3fff;

        // name tables
        if (address >= 0x2000 && address <= 0x3eff) {
            this.writeNameTable(address, value);
        }

        // palette
        else if (address >= 0x3f00 && address <= 0x3fff) {
            this.writePalette(address, value);
        }

        // fallback
        else {
            this.vram[address] = value;
        }
    }

    NES.Console.prototype.writeNameTable = function (address, value) {
        if (this.mirroring === MIRRORING.HORIZONTAL) {
            switch (address & 0x2c00) {
                case 0x2000:
                case 0x2800: {
                    this.vram[address & 0x2fff] = value;
                    this.vram[(address + 0x400) & 0x2fff] = value;
                }; break;

                case 0x2400:
                case 0x2c00: {
                    this.vram[address & 0x2fff] = value;
                    this.vram[(address - 0x400) & 0x2fff] = value;
                }; break;
            }
        } else if (this.mirroring === MIRRORING.VERTICAL) {
            switch (address & 0x2c00) {
                case 0x2000:
                case 0x2400: {
                    this.vram[address & 0x2fff] = value;
                    this.vram[(address + 0x800) & 0x2fff] = value;
                }; break;

                case 0x2800:
                case 0x2c00: {
                    this.vram[address & 0x2fff] = value;
                    this.vram[(address - 0x800) & 0x2fff] = value;
                }; break;
            }
        }
    }

    NES.Console.prototype.writePalette = function (address, value) {
        value &= 0x3f;
        this.vram[address] = value;

        if (address === 0x3f00 || address === 0x3f10) {
            this.vram[0x3f00] = value;
            this.vram[0x3f10] = value;
        } else if (address === 0x3f04 || address === 0x3f14) {
            this.vram[0x3f04] = value;
            this.vram[0x3f14] = value;
        } else if (address === 0x3f08 || address === 0x3f18) {
            this.vram[0x3f08] = value;
            this.vram[0x3f18] = value;
        } else if (address === 0x3f0c || address === 0x3f1c) {
            this.vram[0x3f0c] = value;
            this.vram[0x3f1c] = value;
        }
    }

    NES.Console.prototype.draw = function () {
        var buffer = this.ppu.getScreenBuffer();
        var colour;
        var r;
        var g;
        var b;
        var imageDataPointer;

        for (var index = 0; index < SCREEN.width * SCREEN.height; index++) {
            colour = buffer[index];
            r = (colour >> 16) & 0xff;
            g = (colour >> 8) & 0xff;
            b = colour & 0xff;

            imageDataPointer = index * 4;
            this.screen.imageData.data[imageDataPointer] = r;
            this.screen.imageData.data[imageDataPointer + 1] = g;
            this.screen.imageData.data[imageDataPointer + 2] = b;
            this.screen.imageData.data[imageDataPointer + 3] = 255;
        }

        this.screen.ctx.putImageData(this.screen.imageData, 0, 0);
    }

} ());
