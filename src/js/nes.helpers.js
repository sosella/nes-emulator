var Helper = (function () {
    'use strict';

    return {
        makeWord: makeWord,
        testBit: testBit
    };

    function makeWord (l, h) {
        return ((h << 8) | l) & 0xffff;
    }

    function testBit (value, bit) {
        var mask = (1 << bit) & 0xff;

        return ((value & mask) === mask);
    }

} ());

////////////////////////////////////////////////////////////////////////////////////////////////////

(function () {
    'use strict';

    function toFixedLength (size, rightAlign) {
        var output = this.toString();

        while (output.length < size) {
            if (rightAlign) {
                output = ' ' + output;
            } else {
                output = output + ' ';
            }
        }

        return output;
    }
    Number.prototype.toFixedLength = toFixedLength;
    String.prototype.toFixedLength = toFixedLength;

    function toHexadecimal (length) {
        var output = this.toString(16);
        var size = length || ((output.length <= 2) ? 2 : 4);

        while (output.length < size) {
            output = '0' + output;
        }

        return output.toUpperCase();
    }
    Number.prototype.toHexadecimal = toHexadecimal;

} ());
