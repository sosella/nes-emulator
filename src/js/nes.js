var NES = null;

(function () {
    'use strict';

    NES = function (args) {
        this.console = new NES.Console({
            names: args.names,
            patterns: args.patterns,
            rom: args.rom,
            screen: args.screen
        });

        this.setupEmulator();
        this.console.start();
    }

    NES.prototype.setupEmulator = function () {
        document.addEventListener('keydown', function (event) {
            var key = event.which;

            if (key === KEY.ESCAPE) {
                console.log('EMULATOR STOPPED');
                this.console.stop();
            }
        }.bind(this));
    }

} ());
