(function () {
    'use strict';

    NES.Debugger = function (args) {
        this.ctx = args.m6502;
        this.opcode = 0;
        this.data = null;
        this.operands = [];
        this.totalCycles = 0;
        this.patterns = {
            canvas: document.getElementById(args.patterns),
            height: 256,
            width: 128
        };
        this.names = {
            canvas: document.getElementById(args.names),
            height: 480,
            width: 512
        };

        this.patterns.ctx = this.patterns.canvas.getContext('2d');
        this.patterns.imageData = this.patterns.ctx.createImageData(this.patterns.width, this.patterns.height);
        this.names.ctx = this.names.canvas.getContext('2d');
        this.names.imageData = this.names.ctx.createImageData(this.names.width, this.names.height);
    }

    NES.Debugger.prototype.traceM6502 = function () {
        this.opcode = this.ctx.memory.peekROM(this.ctx.pc);
        this.data = OPCODE_DATA[this.opcode];

        if (!this.data) {
            throw new Error('opcode not defined <' + this.ctx.pc.toHexadecimal(3) + ':' + this.opcode.toHexadecimal() + '>');
        }

        this.operands = this.getOperands();
        this.appendTraceLine();
    }

    NES.Debugger.prototype.getOperands = function () {
        var operands = [];

        for (var i = 0; i < this.data.size; i++) {
            operands.push(this.ctx.memory.peekROM(this.ctx.pc + i));
        }

        return operands;
    }

    NES.Debugger.prototype.getExplanation = function () {
        var output = this.data.name + ' ';
        var address;
        var l;
        var h;

        switch (this.data.mode) {
            case 'Absolute': {
                address = Helper.makeWord(this.operands[1], this.operands[2]);
                output += '$' + address.toHexadecimal();

                if (['JMP', 'JSR'].indexOf(this.data.name) === -1) {
                    output += ' = ' + this.ctx.memory.peekROM(address).toHexadecimal();
                }
            }; break;

            case 'Absolute,X':
            case 'Absolute,X*': {
                address = (Helper.makeWord(this.operands[1], this.operands[2]) + this.ctx.x) & 0xffff;
                output += '$' +
                            this.operands[2].toHexadecimal() +
                            this.operands[1].toHexadecimal() +
                            ',X @ ' +
                            address.toHexadecimal(4) +
                            ' = ' +
                            this.ctx.memory.peekROM(address).toHexadecimal();
            }; break;

            case 'Absolute,Y':
            case 'Absolute,Y*': {
                address = (Helper.makeWord(this.operands[1], this.operands[2]) + this.ctx.y) & 0xffff;
                output += '$' +
                            this.operands[2].toHexadecimal() +
                            this.operands[1].toHexadecimal() +
                            ',Y @ ' +
                            address.toHexadecimal(4) +
                            ' = ' +
                            this.ctx.memory.peekROM(address).toHexadecimal();
            }; break;

            case 'Accumulator': {
                output += 'A';
            }; break;

            case 'Immediate': {
                output += '#$' + this.operands[1].toHexadecimal();
            }; break;

            case 'Indirect': {
                address = Helper.makeWord(this.operands[1], this.operands[2]);
                output += '($' +
                            address.toHexadecimal() +
                            ') = ' +
                            this.ctx.memory.peekROM((address + 1) & 0xffff).toHexadecimal() +
                            this.ctx.memory.peekROM(address).toHexadecimal();
            }; break;

            case 'Relative': {
                address = this.operands[1];

                if (Helper.testBit(address, 7)) {
                    address -= 0x100;
                }

                output += '$' + ((this.ctx.pc + 2 + address) & 0xffff).toHexadecimal();
            }; break;

            case 'Zero Page': {
                output += '$' +
                            this.operands[1].toHexadecimal() +
                            ' = ' +
                            this.ctx.memory.peekROM(this.operands[1]).toHexadecimal();
            }; break;

            case 'Zero Page,X': {
                address = (this.operands[1] + this.ctx.x) & 0xff;
                output += '$' +
                            this.operands[1].toHexadecimal() +
                            ',X @ ' +
                            address.toHexadecimal() +
                            ' = ' +
                            this.ctx.memory.peekROM(address).toHexadecimal();
            }; break;

            case 'Zero Page,Y': {
                address = (this.operands[1] + this.ctx.y) & 0xff;
                output += '$' +
                            this.operands[1].toHexadecimal() +
                            ',Y @ ' +
                            address.toHexadecimal() +
                            ' = ' +
                            this.ctx.memory.peekROM(address).toHexadecimal();
            }; break;

            case '(Indirect,X)': {
                address = ((this.operands[1] + this.ctx.x) & 0xff);
                l = this.ctx.memory.peekROM(address);
                h = this.ctx.memory.peekROM((address + 1) & 0xff);
                output += '($' +
                            this.operands[1].toHexadecimal() +
                            ',X) @ ' +
                            address.toHexadecimal() +
                            ' = ' +
                            Helper.makeWord(l, h).toHexadecimal() +
                            ' = ' +
                            this.ctx.memory.peekROM(Helper.makeWord(l, h)).toHexadecimal();
            }; break;

            case '(Indirect),Y':
            case '(Indirect),Y*': {
                l = this.ctx.memory.peekROM(this.operands[1]);
                h = this.ctx.memory.peekROM((this.operands[1] + 1) & 0xff);
                address = Helper.makeWord(l, h);
                output += '($' +
                            this.operands[1].toHexadecimal() +
                            '),Y = ' +
                            address.toHexadecimal() +
                            ' @ ' +
                            ((address + this.ctx.y) & 0xffff).toHexadecimal(4) +
                            ' = ' +
                            this.ctx.memory.peekROM((address + this.ctx.y) & 0xffff).toHexadecimal();
            }; break;
        }

        return output;
    }

    NES.Debugger.prototype.getRegisters = function () {
        var output = 'A:' + this.ctx.a.toHexadecimal();

        this.totalCycles += (this.ctx.cycles * 3);

        if (this.totalCycles >= 341) {
            this.totalCycles -= 341;
        }

        output += ' X:' + this.ctx.x.toHexadecimal();
        output += ' Y:' + this.ctx.y.toHexadecimal();
        output += ' P:' + this.ctx.status.toHexadecimal();
        output += ' SP:' + this.ctx.sp.toHexadecimal();
        output += ' CYC:' + this.totalCycles.toFixedLength(3, true);

        return output;
    }

    NES.Debugger.prototype.appendTraceLine = function () {
        var output = this.ctx.pc.toHexadecimal().toFixedLength(6);

        output += this.operands
                    .map(function (item) { return item.toHexadecimal() })
                    .join(' ')
                    .toFixedLength(10);

        output += this.getExplanation().toFixedLength(32);
        output += this.getRegisters();

        document.write('<pre>' + output + '</pre>');
    }

    NES.Debugger.prototype.showPatterns = function () {
        var address;
        var colour;
        var imageDataPointer;
        var data = {};
        var pixel = {};
        var tilesPerRow = 16;
        var tileSize = 8;
        var bytesPerPixel = 4;

        for (var tileNumber = 0; tileNumber < 512; tileNumber++) {
            for (var tileLine = 0; tileLine < 8; tileLine++) {
                address = tileNumber * 16 + tileLine;
                data.l = this.ctx.memory.peekVRAM(address);
                data.h = this.ctx.memory.peekVRAM(address + 8);

                for (var bit = 7; bit >= 0; bit--) {
                    pixel.l = (data.l >> bit) & 0x1;
                    pixel.h = (data.h >> bit) & 0x1;
                    colour = PATTERNS_PALETTE[(pixel.h << 1) | pixel.l & 0x3];

                    imageDataPointer = (Math.floor(tileNumber / tilesPerRow) * tileSize + tileLine) * this.patterns.width;
                    imageDataPointer += (tileNumber % tilesPerRow) * tileSize + (7 - bit);
                    imageDataPointer *= bytesPerPixel;

                    this.patterns.imageData.data[imageDataPointer] = colour.r;
                    this.patterns.imageData.data[imageDataPointer + 1] = colour.g;
                    this.patterns.imageData.data[imageDataPointer + 2] = colour.b;
                    this.patterns.imageData.data[imageDataPointer + 3] = 255;
                }
            }
        }

        this.patterns.ctx.putImageData(this.patterns.imageData, 0, 0);
    }

    NES.Debugger.prototype.showNames = function () {
        var nameTableAddress = [0x2000, 0x2400, 0x2800, 0x2c00];
        var patternTableAddress = 0x1000; // FIXME: hardcoded
        var patternAddress;
        var patternNumber;
        var patternData = {l: null, h: null};
        var pixel = {l: null, h: null};
        var colour;
        var x;
        var y;
        var imageDataPointer;

        nameTableAddress.forEach(function (startAddress, nameTableIndex) {
            for (var tileNumber = 0; tileNumber < 30 * 32; tileNumber++) {
                for (var tileLine = 0; tileLine < 8; tileLine++) {
                    patternNumber = this.ctx.memory.peekVRAM((startAddress + tileNumber) & 0xffff);
                    patternAddress = (patternNumber * 16 + tileLine + patternTableAddress) & 0xffff;
                    patternData.l = this.ctx.memory.peekVRAM(patternAddress);
                    patternData.h = this.ctx.memory.peekVRAM((patternAddress + 8) & 0xffff);

                    for (var bit = 7; bit >= 0; bit--) {
                        pixel.l = (patternData.l >> bit) & 0x1;
                        pixel.h = (patternData.h >> bit) & 0x1;
                        colour = PATTERNS_PALETTE[(pixel.h << 1) | pixel.l & 0x3];

                        x = (nameTableIndex % 2) * this.names.width / 2;
                        x += (tileNumber % 32) * 8;
                        x += 7 - bit;

                        y = Math.floor((nameTableIndex / 2)) * this.names.height / 2;
                        y += Math.floor(tileNumber / 32) * 8 + tileLine;

                        imageDataPointer = (y * this.names.width + x) * 4;

                        this.names.imageData.data[imageDataPointer] = colour.r;
                        this.names.imageData.data[imageDataPointer + 1] = colour.g;
                        this.names.imageData.data[imageDataPointer + 2] = colour.b;
                        this.names.imageData.data[imageDataPointer + 3] = 255;
                    }
                }
            }
        }.bind(this));

        this.names.ctx.putImageData(this.names.imageData, 0, 0);
    }

} ());
